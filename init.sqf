ltkCuratorSync = compile preprocessFileLineNumbers "LTK\ltkCuratorSync.sqf";

placements_afb = compile preprocessFileLineNumbers "LTK\create\placements_afb.sqf";
placements = compile preprocessFileLineNumbers "LTK\create\placements.sqf";


if(isServer || isDedicated) then{

	ltkCreatedObjects=[];
	[ltkCreatedObjects] spawn placements;
	[ltkCreatedObjects] spawn ltkCuratorSync;

	[[]] spawn placements_afb;

};
