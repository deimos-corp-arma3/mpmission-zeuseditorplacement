/**

This script will allow you to copy to clipboard the current placement and position of curator Editable Objects
Execute on any unit via init/trigger/other script, replacing [this] with [unitName] or [player] if needed:

nul = [player] execVM "LTK/zeusEditablesToClipboard.sqf";

**/


	private["_curator","_curators","_clipText","_unit", "_editablesObjects"];

	_unit = [_this,0,objnull,[objnull]] call bis_fnc_param;
	_curator = allcurators select 0;
	_editablesObjects = curatorEditableObjects _curator;
	//_toExportList = [];
	_clipText = "if( isNil 'ltkCreatedObjects' ) then {ltkCreatedObjects=[];};";

	{	
		//_toExportList = _toExportList + ['createVehicle', typeOf _x, getPosATL _x, getDir _x];

		if(vehicle _x isKindOf "Man") then
		{
			//_clipText = _clipText + format ["_created = null createUnit ['%1', %2, [], %3, 'FORM']; ",  typeOf _x, [0,0,0], 0];
		}
		else
		{
			_clipText = _clipText + format ["_created = createVehicle ['%1', %2, [], %3, 'NONE']; ", typeOf _x, [0,0,0], 0];
		};

		_clipText = _clipText + format ["_created setPosATL %1; ", getPosATL _x];
		_clipText = _clipText + format ["_created setDir %1; ", getDir _x];
		_clipText = _clipText + "ltkCreatedObjects = ltkCreatedObjects + [_created]; ";

	}forEach _editablesObjects;

	copyToClipboard _clipText;